

import org.junit.Test;

import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();
        String expression = "100+300+44+66+72";
        assertEquals("582", calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals("201", calculatorResource.calculate(expression));
        expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "100+300+44+66+72";
        assertEquals(582, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-90-9";
        assertEquals(900, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "3*3*111";
        assertEquals(999, calculatorResource.multiplication(expression));

        expression = "20*2";
        assertEquals(40, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999/111";
        assertEquals(9, calculatorResource.division(expression));

        expression = "999/111/3";
        assertEquals(3, calculatorResource.division(expression));
    }
}
