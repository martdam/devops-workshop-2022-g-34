package data;
import data.Message;
import data.User;
import java.util.ArrayList;

/**
 * Class for the GroupChat object as saved in database
 * test test test
 */
public class GroupChat {
    private int groupChatId;
    private String groupChatName;
    private ArrayList<Message> messageList = new ArrayList<>();
    private ArrayList<User> userList = new ArrayList<>();

    /**
     * Empty constructor
     */
    public GroupChat() {
    }

    /**
     * Constructor that takes in groupChat id and name as argument
     * @param groupChatId an id that represents the chat
     * @param groupChatName a string name that represents the chat name
     */
    public GroupChat(int groupChatId, String groupChatName) {
        this.groupChatId = groupChatId;
        this.groupChatName = groupChatName;
    }

    public int getGroupChatId() {
        return groupChatId;
    }

    public String getGroupChatName() {
        return groupChatName;
    }

    public ArrayList<Message> getMessageList() {
        return messageList;
    }

    public ArrayList<User> getUserList() {
        return userList;
    }

    public void setGroupChatId(int groupChatId) {
        this.groupChatId = groupChatId;
    }

    public void setGroupChatName(String groupChatName) {
        this.groupChatName = groupChatName;
    }

    public void getGroupChat(){

    }

    public void setMessageList(ArrayList<Message> messageList) {
        this.messageList = messageList;
    }

    public void setUserList(ArrayList<User> userList) {
        this.userList = userList;
    }
}


